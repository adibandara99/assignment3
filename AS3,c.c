#include <stdio.h>
int main()
{
    char c,capital,simple;
    printf("Enter a character: ");
    scanf("%c",&c);

    capital = (c=='A'||c=='E'||c=='I'||c=='O'||c=='U');
    simple  = (c=='a'||c=='e'||c=='i'||c=='o'||c=='u');
    if (simple || capital)
        printf("%c is a vowel",c);
    else
        printf("%c is a consonant",c);
    return 0;
}
